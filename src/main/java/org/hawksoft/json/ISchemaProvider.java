/*
The MIT License (MIT)

Copyright (c) 2013-2017 Russ Jackson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

 */
package org.hawksoft.json;

import org.codehaus.jettison.json.JSONObject;

/**
 * Created at 9:44 AM on 2013-12-16
 *
 * @author Russ Jackson (last updated by $Author: arjackson $)
 * @version $Revision: 11 $, $Date: 2014-01-19 20:09:53 -0600 (Sun, 19 Jan 2014) $
 */
public interface ISchemaProvider {

    /**
     * lookup the schema with the given namespace, schemaId and version
     *
     * @param namespace namespace to match
     * @param schemaId schema Id to match
     * @param version schema version to match
     * @return schema document as JSONObject, if one exists, null otherwise
     */
    public JSONObject getSchema(String namespace, String schemaId, String version);

} // interface ISchemaProvider
