package org.hawksoft.json;

/**
 * Created at 6:26 PM on 2013-12-15
 *
 * @author Russ Jackson (last updated by $Author: arjackson $)
 * @version $Revision: 20 $, $Date: 2014-02-23 10:10:47 -0600 (Sun, 23 Feb 2014) $
 */
public class JSONValidationException extends Exception {

    /**
     * construct exception with message only
     *
     * @param message description of cause of exception
     */
    public JSONValidationException(String message) {
        super(message);
    } // constructor

    /**
     * construct exception by wrapping another exception
     *
     * @param e original exception to wrap
     */
    public JSONValidationException(Exception e) {
        super(e);
    } // constructor

    /**
     * construct exception by wrapping root exception and providing custom message
     *
     * @param message description of cause of exception
     * @param e original exception to wrap
     */
    public JSONValidationException(String message, Exception e) {
        super(message, e);
    } // constructor

} // class JSONValidationException
