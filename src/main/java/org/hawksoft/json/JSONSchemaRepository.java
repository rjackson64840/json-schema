/*
The MIT License (MIT)

Copyright (c) 2013-2017 Russ Jackson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

 */
package org.hawksoft.json;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static java.lang.String.format;

/**
 * Created at 5:37 PM on 2013-12-20
 *
 * @author Russ Jackson (last updated by $Author: arjackson $)
 * @version $Revision: 48 $, $Date: 2014-08-10 10:10:59 -0500 (Sun, 10 Aug 2014) $
 */
public class JSONSchemaRepository implements ISchemaProvider {

    // private static final Logger LOG = LoggerFactory.getLogger(JSONSchemaRepository.class);

    // indent factor for JSONObject pretty print
    private static final int INDENT = 4;

    private static final String DOCUMENT = "document";

    // mapping of schemas maintained by this repository
    private Map<String, JSONObject> _schemaMap = new HashMap<>();

    // mapping of examples extracted from the given schemas
    private Map<String, String> _examples = null;

    /**
     * obtain a mapping of all schemas maintained by this repository
     *
     * @return Map of schema name/schema key/value pairs
     */
    public Map<String, String> getAllSchemas() {
        Map<String, String> result = new HashMap<>();
        for (Map.Entry<String, JSONObject> entry : _schemaMap.entrySet()) {
            try {
                result.put(entry.getKey(), entry.getValue().toString(INDENT));
            } catch (JSONException e) {
                LOG.error("error building json for {}", entry.getKey());
            }
        }
        return result;
    } // getAllSchemas()

    /**
     * obtain a mapping of all example documents derived from the schemas
     * maintained by this repository
     *
     * @return mapping of schema names to document examples, both as strings
     */
    public Map<String, String> getAllExamples() throws JSONValidationException {
        if (null == _examples) {
            _examples = new HashMap<>();
            for (Map.Entry<String, JSONObject> entry : _schemaMap.entrySet()) {
                try {
                    _examples.put(entry.getKey(), getExample(entry.getValue()).toString(INDENT));
                } catch (JSONException e) {
                    String msg = format("error building json example for %s", entry.getKey());
                    LOG.error(msg);
                    throw new JSONValidationException(msg, e);
                }
            }
        }
        return _examples;
    } // getAllExamples()

    /**
     * construct a schema repository for the given resource path;  all
     * JSON schema documents (file type 'jsd') will be added to the repository.
     *
     * each document will be loaded into a JSONObject and the top-level
     * 'document' object will be check and used to create the identifier
     * for the schema
     *
     * @param repoPath
     * @throws JSONValidationException
     */
    public JSONSchemaRepository(String repoPath) throws JSONValidationException {
        PathMatchingResourcePatternResolver resolver =
            new PathMatchingResourcePatternResolver();

        try {
            Resource[] resources = resolver.getResources(repoPath + "/*.jsd");

            for (Resource resource : resources) {
                JSONObject schema = JSONSchemaValidator.loadAndValidateSchema(
                    resource.getInputStream()
                );

                _schemaMap.put(
                    getSchemaIdentifier(schema.getJSONObject(DOCUMENT)),
                    schema
                );
            }
        } catch (IOException | JSONException e) {
            throw new JSONValidationException(e);
        }
    } // constructor

    /*
        build the schema identifier from the given JSONObject, extracting the
        necessary attributes
     */
    private String getSchemaIdentifier(JSONObject json) {
        return getSchemaIdentifier(
            json.optString("namespace"),
            json.optString("id"),
            json.optString("version")
        );
    } // getSchemaIdentifier()

    /*
        build the schema identifier using the namespace, id, and version
        of the document
     */
    private String getSchemaIdentifier(String namespace,
                                       String id,
                                       String version) {
        return format(
            "application/%s-%s-%s+json",
            namespace,
            id,
            version
        );
    } // getSchemaIdentifier()

    /**
     * obtain the schema for the given namespace, schema id and version
     *
     * @param namespace schema namespace
     * @param schemaId schema identifier
     * @param version schema version
     * @return JSONObject representation of the schema
     */
    @Override
    public JSONObject getSchema(String namespace, String schemaId, String version) {
        return _schemaMap.get(getSchemaIdentifier(namespace, schemaId, version));
    } // getSchema()

    /**
     * obtain the schema that matches the given document
     *
     * @param dataDoc JSON data document
     * @return JSON schema document
     */
    public JSONObject getSchema(JSONObject dataDoc) throws JSONValidationException {
        if (null == dataDoc) {
            throw new IllegalArgumentException("data doc is required");
        }

        JSONObject result = null;
        try {
            JSONObject docSection = dataDoc.getJSONObject(DOCUMENT);
            result = getSchema(
                docSection.getString("namespace"),
                docSection.getString("id"),
                docSection.getString("version")
            );
        } catch (JSONException e) {
            throw new JSONValidationException(e.getMessage());
        }

        return result;
    } // getSchema()

    /**
     * extract an example data document from the given schema document
     *
     * @param schema JSONObject representing the schema to extract an example from
     * @return JSONObject example data document
     */
    public JSONObject getExample(JSONObject schema) throws JSONValidationException {
        JSONObject example = null;
        try {
            example = new JSONObject(schema.toString());
            JSONObject doc = example.getJSONObject(DOCUMENT);
            doc.put("type", "instance");
            doc.remove("model");
            extractExample(example, DOCUMENT);
        } catch (JSONException e) {
            LOG.error("", e);
            throw new JSONValidationException(e);
        }

        return example;
    } // getExample()

    /**
     * obtain an example data document for the schema document described
     * by the given namespace, id and version
     *
     * @param namespace document namespace
     * @param schemaId schema identifier
     * @param version schema version
     * @return JSONObject example data document
     */
    public JSONObject getExample(String namespace, String schemaId, String version)
        throws JSONValidationException {
        return getExample(getSchema(namespace, schemaId, version));
    } // getExample()

    /*
        extract an example document from the given schema, ignoring the attribute
        matching the provided name
     */
    private JSONObject extractExample(JSONObject json, String ignores)
        throws JSONException, JSONValidationException {
        JSONArray array = json.names();
        for (int i = 0; i < array.length(); i++) {
            String attributeName = array.getString(i);
            if (ignores.equalsIgnoreCase(attributeName)) {
                continue;
            }
            Object attribute = json.get(attributeName);
            if (attributeName.contains("?")) {
                // remove the '?' from the attribute name
                json.remove(attributeName);
                attributeName = attributeName.substring(0, attributeName.indexOf('?'));
                json.put(attributeName, attribute);
            }

            if (attribute instanceof JSONObject) {
                extractExample((JSONObject)attribute, "");
            } else if (attribute instanceof JSONArray) {
                extractExample((JSONArray)attribute);
            } else {
                json.put(attributeName, extractExample((String)attribute));
            }
        }

        return json;
    } // extractExample()

    /*
        extract the individual attributes from the validation string
     */
    private Map<String, Object> getAttributes(String attributes)
        throws JSONValidationException {
        Map<String, Object> result = new HashMap<>();
        String[] parts = attributes.split(";");
        for (String part : parts) {
            String[] keyValue = part.split("=");
            if (keyValue.length != 2) {
                throw new JSONValidationException(
                    format("invalid schema attribute entry '%s'", attributes)
                );
            }
            result.put(keyValue[0], keyValue[1]);
        }
        String type = (String)result.get("type");
        if (StringUtils.isNotEmpty(type) && "bool".equalsIgnoreCase(type)) {
            String example = (String)result.get("example");
            result.put("example", Boolean.valueOf(example));
        }
        return result;
    } // getAttributes()

    /*
        extract the example value
     */
    private Object extractExample(String attributes) throws JSONValidationException {
        Map<String, Object> map = getAttributes(attributes);
        return map.get("example");
    } // extractExample()

    /*
        extract an example value from the given array
     */
    private JSONArray extractExample(JSONArray array)
        throws JSONException, JSONValidationException {
        Object value = array.get(0);
        array.remove(value);
        if (value instanceof JSONObject) {
            array.put(extractExample((JSONObject)value, ""));
        } else if (value instanceof JSONArray) {
            array.put(extractExample((JSONArray)value));
        } else {
            array.put(extractExample((String)value));
        }
        return array;
    } // extractExample()

} // class JSONSchemaRepository
