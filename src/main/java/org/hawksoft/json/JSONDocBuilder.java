/*
The MIT License (MIT)

Copyright (c) 2013-2017 Russ Jackson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

 */
package org.hawksoft.json;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.lang.String.format;

/**
 * Created at 1:27 PM on 2017-04-26
 *
 * @author Russ Jackson
 */
public class JSONDocBuilder {

    // private static final Logger LOG = LoggerFactory.getLogger(JSONDocBuilder.class);

    private JSONDocBuilder() {
        // default constructor
    } // constructor

    public static JSONObject startDocument(String namespace, String id, String version) {
        assertRequiredParam("namespace", namespace);
        assertRequiredParam("id", id);
        assertRequiredParam("version", version);

        JSONObject result = new JSONObject();

        JSONObject doc = new JSONObject();
        try {
            result.put("document", doc);
            doc.put("type", "instance");
            doc.put("namespace", namespace);
            doc.put("id", id);
            doc.put("version", version);
        } catch (JSONException e) {
            throw new RuntimeException(e.getMessage(), e);
        }

        return result;
    } // startDocument()

    private static void assertRequiredParam(String name, String value) {
        if (StringUtils.isBlank(value)) {
            throw new IllegalArgumentException(format("required param '%s' is missing", name));
        }
    } // assertRequiredParam()

} // class JSONDocBuilder
