/*
The MIT License (MIT)

Copyright (c) 2013-2017 Russ Jackson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

 */
package org.hawksoft.json;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.String.format;

/**
 * Created at 4:16 PM on 2013-12-15
 *
 * @author Russ Jackson (last updated by $Author: arjackson $)
 * @version $Revision: 49 $, $Date: 2014-08-10 10:32:13 -0500 (Sun, 10 Aug 2014) $
 */
public final class JSONSchemaValidator {

     private static final Logger LOG = LoggerFactory.getLogger(JSONSchemaValidator.class);

    private static final Pattern UUID_PATTERN = Pattern.compile(
        "[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}"
    );

    private static final Set<String> NATIVE_TYPES = new HashSet<>(
        Arrays.asList(new String[]{"int","long","date","double","enum","bool","string","uuid"})
    );

    private static final String MAX = "max";

    private static final String MIN = "min";

    private static final String REGEX = "regex";

    private static final String TYPE = "type";

    private static final String USER_TYPES = "userTypes";

    private static final String IMPORTS = "imports";

    private static final String DOCUMENT = "document";

    private static final Set<String> MODEL_SET = new HashSet<>();
    static {
        MODEL_SET.add("1311.0");
        MODEL_SET.add("1312.0");
    }

    private Map<String, Map<String, String>> _userDefinedTypes = new HashMap<>();
    private ISchemaProvider _schemaProvider;

    private boolean _isStrict = true;

    private JSONSchemaValidator() {
    } // constructor

    /**
     * validate a json document against a matching schema, if one exists;
     *
     * use this method if you have a JSONObject document and you want the
     * matching schema to be looked up from the given provider
     *
     * @param json the document to be validated
     * @param schemaProvider a schema provider that can be used to locate the
     *                       correct schema for the given document
     * @throws JSONValidationException thrown if the schema is invalid or if
     * the document doesn't match the schema
     */
    public static void validateDocument(JSONObject json,
                                        ISchemaProvider schemaProvider)
        throws JSONValidationException {
        JSONSchemaValidator validator = new JSONSchemaValidator();
        validator.validateDocTypeDocument(json, schemaProvider);
    } // validateDocument()

    /**
     * validate the given json document against the given schema
     *
     * use this method if you have a json document and the matching schema
     *
     * @param json the document to be validated
     * @param schema the schema to use to validate the document
     * @throws JSONValidationException  thrown if the schema is invalid or if
     * the document doesn't match the schema
     */
    public static void validateDocument(JSONObject json, final JSONObject schema)
        throws JSONValidationException {
        validateDocument(json, new ISchemaProvider() {
            @Override
            public JSONObject getSchema(String namespace,
                                        String schemaId,
                                        String version) {
                return schema;
            }
        });
    } // validateDocument()

    /**
     * validates the given document against the given schema;  both the document
     * and the schema will be converted to JSONObjects before further processing,
     * so the documents should be valid json as acceptable by JSONObject
     *
     * use this method if you have string representations of both the schema and
     * the document;
     *
     * @param json string representing the json document to be validated
     * @param schema string representing the schema used to validate the
     *               given document
     * @throws JSONValidationException thrown if either document fails to convert
     * into a JSONObject, if the schema is invalid, or if the json document doesn't
     * match the provided schema
     */
    public static void validateDocument(String json, String schema)
        throws JSONValidationException {
        try {
            validateDocument(new JSONObject(json), new JSONObject(schema));
        } catch (JSONException e) {
            throw new JSONValidationException(e);
        }
    } // validateDocument()

    /**
     * validate the given json object with a schema, if one exists, that will be
     * looked up from the given provider
     *
     * use this method if you have a string representing a json document and you
     * want the corresponding schema to be looked up using the given provider
     *
     * @param json string representing the json document to be validated
     * @param schemaProvider a schema provider that can be used to locate the
     *                       correct schema for the given document
     * @throws JSONValidationException thrown if the given string cannot be
     * converted to a JSONObject, a schema cannot be located, the schema is
     * invalid or the document doesn't match the schema
     */
    public static void validateDocument(String json, ISchemaProvider schemaProvider)
        throws JSONValidationException {
        try {
            validateDocument(new JSONObject(json), schemaProvider);
        } catch (JSONException e) {
            throw new JSONValidationException(e);
        }
    } // validateDocument()

    /**
     * validate a json document that is exposed via an InputStream using the
     * given schema provider to look up the associated schema
     *
     * @param json the InputStream representing the json document
     * @param schemaProvider a schema provider that can be used to locate the
     *                       correct schema for the given document
     * @throws JSONValidationException thrown if there is a problem with the
     * input stream, if the text in the stream cannot be converted to a
     * JSONObject, if a matching schema cannot be located, if the schema is
     * invalid, or if the document does not match the schema
     */
    public static void validateDocument(InputStream json,
                                        ISchemaProvider schemaProvider)
        throws JSONValidationException {

        validateDocument(loadDocument(json), schemaProvider);
    } // validateDocument()


    /**
     * validates the 'document' header for the given schema document
     *
     * @param schema a json document representing a schema
     * @throws JSONValidationException thrown if the schema does not have a valid
     * document header
     */
    public static void validateSchema(JSONObject schema)
        throws JSONValidationException {
        JSONSchemaValidator validator = new JSONSchemaValidator();

        Stack<String> tracker = new Stack<>();
        try {
            validator.validateDocTypeSchema(schema, tracker);
        } catch(JSONValidationException e) {
            String msg = validator.unwind(tracker);
            LOG.error("Error validating schema at : " + msg);
            System.out.println("Error validating schema at : " + msg);
            throw(e);
        }
    } // validateSchema()

    /**
     * loads a JSONObject representing a schema from an InputStream and
     * validates the 'document' header for the schema
     *
     * @param is InputStream representing the schema document
     * @return the schema document loaded from the input stream
     * @throws JSONValidationException thrown if there is a problem reading
     * the document from the stream, if the schema cannot be converted to a
     * JSONObject or if the schema does not have a valid 'document' header
     */
    public static JSONObject loadAndValidateSchema(InputStream is)
        throws JSONValidationException {
        JSONObject result = null;
        try {
            result = new JSONObject(loadDocument(is));
            validateSchema(result);
        } catch (JSONException e) {
            throw new JSONValidationException(e);
        }
        return result;
    } // loadAndValidateSchema()

    /*
        load a document from the input stream into a String;  IOException
        is wrapped in JSONValidationException
     */
    private static String loadDocument(InputStream is)
        throws JSONValidationException {
        String result = null;

        try {
            StringBuilder sb = new StringBuilder();
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            String line = null;

            while (null != (line = reader.readLine())) {
                sb.append(line);
            }
            reader.close();
            result = sb.toString();
        } catch (IOException e) {
            throw new JSONValidationException(e);
        }

        return result;
    } // loadDocument()

    /*
        assert that the given JSONObject has a JSONObject child of the given name
     */
    private JSONObject assertChildObjectExists(JSONObject json, String childName)
        throws JSONValidationException {
        JSONObject child = null;
        try {
            child = json.getJSONObject(childName);
        } catch (JSONException e) {
            throw new JSONValidationException(
                format("required child object '%s' is missing", childName),
                e
            );
        }
        return child;
    } // assertChildObjectExists()

    /*
        assert that the given schema document model is supported by this implementation
     */
    private void assertModelSupported(String model) throws JSONValidationException {
        if (!MODEL_SET.contains(model)) {
            throw new JSONValidationException(
                format("model '%s' not supported", model)
            );
        }
    } // assertModelSupported()

    /*
        assert that a string value with the given key exists in the provided
        JSONObject and that its value matches the given expected value
     */
    private JSONObject assertStringEquals(JSONObject json,
                                          String key,
                                          String expectedValue)
        throws JSONValidationException {
        String value = assertStringExists(json, key);

        if (!value.equalsIgnoreCase(expectedValue)) {
            throw new JSONValidationException(
                format(
                    "value '%s' for property '%s' does not match expected value '%s'",
                    value,
                    key,
                    expectedValue
                )
            );
        }
        return json;
    } // assertStringEquals()

    /*
        assert that a string attribute with the given key name exists in the
        given JSONObject
     */
    private String assertStringExists(JSONObject json, String key)
        throws JSONValidationException {
        String result = null;
        try {
            result = json.getString(key);
            if (null == result || result.trim().isEmpty()) {
                throw new JSONValidationException(
                    format("required property '%s' is blank", key)
                );
            }
        } catch (JSONException e) {
            throw new JSONValidationException(
                format("error accessing string property '%s'", key),
                e
            );
        }
        return result;
    } // assertStringExists()

    /*
        strip any schema notation from the given attribute name
     */
    private String getRealAttributeName(String attribute) {
        return attribute.split("[?]")[0];
    } // getRealAttributeName()

    /*
        parse the given json attribute schema and return the elements as a
        mapping of key-value pairs
     */
    private Map<String, String> getValidations(String attributeName, String schema)
        throws JSONValidationException {
        String[] attributes = schema.split("[;]");
        Map<String, String> result = new HashMap<>();

        for (String attribute : attributes) {
            String[] keyValue = attribute.split("[=]");
            if (keyValue.length != 2) {
                throw new JSONValidationException(
                    format("invalid schema attribute entry '%s'", attribute)
                );
            }
            result.put(keyValue[0], keyValue[1]);
        }

        return result;
    } // getValidations()

    /*
        if the validation contains a reference to a user defined type include
        the validations from that type;  note, only those validation attributes that
        don't already exist will be included
     */
    private String addUserType(String attribute,
                               String type,
                               Map<String, String> validations)
        throws JSONValidationException {
        String resultType = type;

        Map<String, String> udt = _userDefinedTypes.get(type);
        if (null != udt) {
            // inherit 'type' from user defined type
            resultType = udt.get(TYPE);
            validations.put(TYPE, resultType);
            for (Map.Entry<String, String> entry : udt.entrySet()) {
                // only include those validation attributes that don't already exist
                if (!validations.containsKey(entry.getKey())) {
                    validations.put(entry.getKey(), entry.getValue());
                }
            }
        } else {
            throw new JSONValidationException(
                format(
                    "user defined type '%s' referenced by attribute '%s' not found",
                    type,
                    attribute
                )
            );
        }

        return resultType;
    } // addUserType()

    /*
        determine whether the given attribute is optional;  optional attributes
        have a '?' in the name
     */
    private boolean isOptional(String attribute) {
        return attribute.contains("?");
    } // isOptional()

    /*
        validate the given JSONObject representing a data document against the
        given JSONObject representing a schema;  skipDoc should be set to true
        if the given objects represent the root of the json documents since the
        'document' header has its own validation rules
     */
    private void validateObject(JSONObject doc, JSONObject schema, boolean skipDoc, Stack<String> tracker)
        throws JSONValidationException {
        try {
            JSONArray schemaChildren = schema.names();

            Set<String> schemaAttributes = new HashSet<>();

        /*
            iterate over all of the child attributes of the given schema object and
            match them against the given json data object
         */
            for (int i = 0; i < schemaChildren.length(); i++) {
                String childAttribute = schemaChildren.getString(i);
                // build a list of schema attributes so we can later check
                // for extraneous attributes in the json data object
                schemaAttributes.add(childAttribute);
                if (!skipDoc || !childAttribute.equalsIgnoreCase(DOCUMENT)) {
                    tracker.push(childAttribute);
                    validateAttribute(
                        doc,
                        schema,
                        childAttribute,
                        tracker
                    );
                    tracker.pop();
                }
            }

        /*
            iterate over all of the json data object attributes and make sure
            they are all in the schema object;  build a list of extraneous
            entries so we can report them later
         */
            StringBuilder sb = new StringBuilder();
            JSONArray docChildren = doc.names();
            if (docChildren.length() > schemaChildren.length()) {
                for (int i = 0; i < docChildren.length(); i++) {
                    String docAttribute = docChildren.getString(i);
                    if (!schemaAttributes.contains(docAttribute)) {
                        if (sb.length() > 0) {
                            sb.append(", ");
                        }
                        sb.append(docAttribute);
                    }
                }
            }

        /*
            if there are any extraneous entries in the json data document
            throw an exception with the list of extra attributes
         */
            if (_isStrict && sb.length() > 0) {
                throw new JSONValidationException(
                    format(
                        "the document contains the following extraneous attribute(s): %s",
                        sb.toString()
                    )
                );
            }
        } catch (JSONException e) {
            throw new JSONValidationException("error processing object", e);
        }

    } // validateObject()

    /*
        validate the given JSONArray data against the given JSONArray representing
        the schema;  the schema array should contain one and only one element
        representing the type of data contained in the array and any constraints
        pertaining to that data
     */
    private void validateArray(JSONArray docArray, JSONArray schemaArray, Stack<String> tracker)
        throws JSONValidationException {
        if (schemaArray.length() != 1) {
            throw new JSONValidationException(
                "array element schema must contain one and only one entry"
            );
        }

        Object oSchema = null;
        try {
            oSchema = schemaArray.get(0);
        } catch (JSONException e) {
            throw new JSONValidationException(
                "invalid schema array definition",
                e
            );
        }

        // check and validate the type contained in the JSONArray
        if (oSchema instanceof JSONObject) {
            validateArrayObjects(docArray, (JSONObject)oSchema, tracker);
        } else if (oSchema instanceof JSONArray) {
            validateNestedArray(docArray, (JSONArray)oSchema, tracker);
        } else {
            validateArrayValues(docArray, (String)oSchema, tracker);
        }
    } // validateArray()

    /*
        validate the values in the given array against the given array element schema
     */
    private void validateArrayValues(JSONArray docArray, String schema, Stack<String> tracker)
        throws JSONValidationException {
        for (int i = 0; i < docArray.length(); i++) {
            try {
                tracker.push(format("[%d]", i));
                validateValue("", docArray.get(i), schema);
                tracker.pop();
            } catch (JSONException e) {
                throw new JSONValidationException(
                    "error processing json array",
                    e
                );
            }
        }
    } // validateArrayValues()

    /*
        validate a nested array against the given array schema
     */
    private void validateNestedArray(JSONArray docArray, JSONArray oSchema, Stack<String> tracker)
        throws JSONValidationException {
        for (int i = 0; i < docArray.length(); i++) {
            try {
                // validate each nested array element against the schema
                tracker.push(format("[%d]", i));
                validateArray(docArray.getJSONArray(i), oSchema, tracker);
                tracker.pop();
            } catch (JSONException e) {
                throw new JSONValidationException(
                    "invalid nested array in array",
                    e
                );
            }
        }
    } // validateNestedArray()

    /*
        validate the objects in the given array against the given schema
     */
    private void validateArrayObjects(JSONArray docArray, JSONObject oSchema, Stack<String> tracker)
        throws JSONValidationException {
        for (int i = 0; i < docArray.length(); i++) {
            try {
                // validate each array object against the schema
                tracker.push(format("[%d]", i));
                validateObject(docArray.getJSONObject(i), oSchema, false, tracker);
                tracker.pop();
            } catch (JSONException e) {
                throw new JSONValidationException(
                    "invalid object in array",
                    e
                );
            }
        }
    } // validateArrayObjects()

    /*
        validate the given document attribute as an object against the given schema
     */
    private void validateObject(JSONObject doc,
                                String docAttribute,
                                JSONObject schemaChild,
                                boolean optional,
                                Stack<String> tracker)
        throws JSONValidationException {
        JSONObject docChild = null;
        try {
            // find the corresponding object in the document
            docChild = doc.getJSONObject(docAttribute);
            // validate the object against the schema;  set skipDoc to false
            // since we're past root object validation
            validateObject(docChild, schemaChild, false, tracker);
        } catch (JSONException e) {
            if (!optional) {
                throw new JSONValidationException(
                    format(
                        "required object '%s' is missing",
                        docAttribute
                    ),
                    e
                );
            }
        }
    } // validateObject()

    /*
        validate the given document attribute as an array against the given schema
     */
    private void validateArray(JSONObject doc,
                               String docAttribute,
                               JSONArray schemaArray,
                               boolean optional,
                               Stack<String> tracker)
        throws JSONValidationException {
        JSONArray docArray = null;

        try {
            docArray = doc.getJSONArray(docAttribute);
            validateArray(docArray, schemaArray, tracker);
        } catch (JSONException e) {
            if (!optional) {
                throw new JSONValidationException(
                    format(
                        "required array '%s' is missing",
                        docAttribute
                    ),
                    e
                );
            }
        }
    } // validateArray()

    /*
        main entry point for attribute validation;  this is meant to be
        recursive;  determine whether the attribute is another object, an array,
        or a value and invoke the appropriate validation
     */
    private void validateAttribute(JSONObject doc,
                                   JSONObject schema,
                                   String attributeName,
                                   Stack<String> tracker)
        throws JSONValidationException {
        Object oAttribute = null;
        try {
            oAttribute = schema.get(attributeName);
        } catch (JSONException e) {
            // this shouldn't happen, but just in case
            LOG.error("unexpected error", e);
            throw new JSONValidationException(e);
        }
        // strip out any meta-data from the attribute name
        String docAttribute = getRealAttributeName(attributeName);
        boolean optional = isOptional(attributeName);

        if (oAttribute instanceof JSONObject) {
            validateObject(doc, docAttribute, (JSONObject)oAttribute, optional, tracker);
        } else if (oAttribute instanceof JSONArray) {
            validateArray(doc, docAttribute, (JSONArray)oAttribute, optional, tracker);
        } else {
            try {
                Object value = doc.get(docAttribute);
                if (!optional || (null != value && !"null".equalsIgnoreCase(value.toString()))) {
                    validateValue(docAttribute, doc.get(docAttribute), (String)oAttribute);
                }
            } catch (JSONException e) {
                if (!optional) {
                    throw new JSONValidationException(
                        format("error accessing attribute '%s'", docAttribute),
                        e
                    );
                }
            }
        }
    } // validateAttribute()

    /*
        validate that the given attribute with the given value is a valid boolean
     */
    private void validateBoolean(String attribute, Object value)
        throws JSONValidationException {
        if (!(value instanceof Boolean)) {
            throw new JSONValidationException(
                format(
                    "%s with value %s is not of type bool (boolean)",
                    attribute,
                    value
                )
            );
        }
    } // validateBoolean()

    /*
        extract the date format type from the schema validations
     */
    private String getDateFormatType(Map<String, String> validations) {
        String result = validations.get("format");
        if (null == result) {
            // default format for date
            result = "date";
        }
        return result;
    } // getDateFormatType()

    /*
        lookup the date format given the supplied type, or a default if the type
        doesn't map to a pre-defined value
     */
    private DateFormat getDateFormat(String formatType) {
        DateFormat result = null;
        switch (formatType) {
            case "date" :
                result = new SimpleDateFormat("yyyy-MM-dd");
                break;

            case "datetime" :
                result = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSS Z");
                break;

            case "time" :
                result = new SimpleDateFormat("HHmm");
                break;

            default :
                // a custom date format was supplied
                result = new SimpleDateFormat(formatType);
                break;
        }
        return result;
    } // getDateFormat()

    /*
        validate the date against a validation minimum, if one exists
     */
    private void validateMinDate(String format,
                                 DateFormat df,
                                 Date date,
                                 String attribute,
                                 Map<String, String> validations)
        throws JSONValidationException {
        // apply a minimum date validation if one is present
        String min = validations.get(MIN);
        if (null != min) {
            try {
                Date minDate = df.parse(min);
                if (date.before(minDate)) {
                    throw new JSONValidationException(
                        format(
                            "%s value '%s' for '%s' is before min %1$s '%s'",
                            format,
                            df.format(date),
                            attribute,
                            df.format(minDate)
                        )
                    );
                }
            } catch (ParseException e) {
                throw new JSONValidationException(
                    format(
                        "invalid min date schema definition '%s' for '%s'",
                        min,
                        attribute
                    ),
                    e
                );
            }
        }
    } // validateMinDate()

    /*
        validate the date against a validation maximum, if one exists
     */
    private void validateMaxDate(String format,
                                 DateFormat df,
                                 Date date,
                                 String attribute,
                                 Map<String, String> validations)
        throws JSONValidationException {
        // apply a maximum date validation if one is present
        String max = validations.get(MAX);
        if (null != max) {
            try {
                Date maxDate = df.parse(max);
                if (date.after(maxDate)) {
                    throw new JSONValidationException(
                        format(
                            "%s value '%s' for '%s' is after max %1$s '%s'",
                            format,
                            df.format(date),
                            attribute,
                            df.format(maxDate)
                        )
                    );
                }
            } catch (ParseException e) {
                throw new JSONValidationException(
                    format(
                        "invalid max date schema definition '%s' for '%s'",
                        max,
                        attribute
                    ),
                    e
                );
            }
        }
    } // validateMaxDate()

    /*
        validate that the given attribute with the given value represents a
        valid date and meets any further validations provided in the given
        validation mapping
     */
    private void validateDate(String attribute,
                              Object value,
                              Map<String, String> validations)
        throws JSONValidationException {
        // dates are expected to be expressed as strings
        if (value instanceof String) {
            String format = getDateFormatType(validations);
            DateFormat df = getDateFormat(format);

            try {
                Date date = df.parse((String)value);
                validateMinDate(format, df, date, attribute, validations);
                validateMaxDate(format, df, date, attribute, validations);
            } catch (ParseException e) {
                // will be used for an example of what the expected date should look like
                Date date = new Date();
                throw new JSONValidationException(
                    format(
                        "date value '%s' for '%s' does not match required format '%s'",
                        value,
                        attribute,
                        df.format(date)
                    ),
                    e
                );
            }
        } else {
            throw new JSONValidationException(
                format(
                    "'%s' is a date and must be of type string; found %s",
                    attribute,
                    value
                )
            );
        }
    } // validateDate()

    /*
        validate that the given JSONObject has an appropriate 'document' header
        for an instance type document
     */
    private JSONObject validateDocTypeDocument(JSONObject json,
                                               ISchemaProvider schemaProvider)
        throws JSONValidationException {
        _schemaProvider = schemaProvider;

        JSONObject doc = assertChildObjectExists(json, DOCUMENT);
        assertStringEquals(doc, TYPE, "instance");

        String namespace = assertStringExists(doc, "namespace");
        String id = assertStringExists(doc, "id");
        String version = assertStringExists(doc, "version");

        JSONObject schema = schemaProvider.getSchema(namespace, id, version);
        if (null == schema) {
            throw new JSONValidationException(
                format("schema missing for '%s-%s-%s'", namespace, id, version)
            );
        }

        JSONObject schemaDoc = null;
        try {
            schemaDoc = schema.getJSONObject(DOCUMENT);
            if (schemaDoc.has("strict")) {
                try {
                    _isStrict = schemaDoc.getBoolean("strict");
                } catch (JSONException e) {
                    throw new JSONValidationException(e.getMessage(), e);
                }
            }
        } catch (JSONException e) {
            // cannot happen due to other checks above, but...
            throw new JSONValidationException("schema missing document section");
        }

        Stack<String> tracker = new Stack<>();

        try {

            // extract the user defined types into an internal map for later reference
            extractUserDefinedTypes(schemaDoc, tracker);

            // extract same from any nested import schemas
            extractUserDefinedTypesFromImports(schemaDoc, tracker);


            validateObject(json, schema, true, tracker);
        } catch(JSONValidationException e) {
            String path = unwind(tracker);
            LOG.error("Document error at : " + path);
            System.out.println("error at : " + path);
            throw(e);
        }

        return doc;
    } // validateDocTypeDocument()

    private String unwind(Stack<String> tracker) {
        StringBuilder sb = new StringBuilder();

        for (String entry : tracker) {
            if (sb.length() > 0) {
                sb.append('.');
            }
            sb.append(entry);
        }

        return sb.toString();
    } // unwind()

    /*
        check all the schema imports for user defined types
     */
    private void extractUserDefinedTypesFromImports(JSONObject schemaDoc, Stack<String> tracker)
        throws JSONValidationException {
        if (schemaDoc.has(IMPORTS)) {
            try {
                JSONArray imports = schemaDoc.getJSONArray(IMPORTS);
                for (int i = 0; i < imports.length(); i++) {
                    tracker.push(format("import[%d]", i));
                    JSONObject oImport = imports.getJSONObject(i);
                    JSONObject iSchema = _schemaProvider.getSchema(
                        oImport.getString("namespace"),
                        oImport.getString("id"),
                        oImport.getString("version")
                    );
                    JSONObject iSchemaDoc = iSchema.getJSONObject(DOCUMENT);
                    extractUserDefinedTypes(iSchemaDoc, tracker);
                    // recursive call to get nested imports
                    extractUserDefinedTypesFromImports(iSchemaDoc, tracker);
                    tracker.pop();
                }
            } catch (JSONException e) {
                LOG.error("error extracting user defined types", e);
                throw new JSONValidationException(
                    "error extracting user defined types",
                    e
                );
            }
        }
    } // extractUserDefinedTypesFromImports()

    /*
        validate that the given JSONObject has an appropriate 'document' header for
        a schema type document
     */
    private JSONObject validateDocTypeSchema(JSONObject json, Stack<String> tracker)
        throws JSONValidationException {
        tracker.push(DOCUMENT);
        JSONObject doc = assertChildObjectExists(json, DOCUMENT);
        assertStringEquals(doc, TYPE, "schema");
        assertModelSupported(assertStringExists(doc, "model"));

        assertStringExists(doc, "namespace");
        assertStringExists(doc, "id");
        assertStringExists(doc, "version");

        extractUserDefinedTypes(doc, tracker);

        tracker.pop();

        return doc;
    } // validateDocTypeSchema()

    /*
        extract any user defined types, performing necessary validations along the way
     */
    private void extractUserDefinedTypes(JSONObject doc, Stack<String> tracker)
        throws JSONValidationException {
        if (doc.has(USER_TYPES)) {
            try {
                JSONObject types = doc.getJSONObject(USER_TYPES);
                JSONArray attributes = types.names();
                for (int i = 0; i < attributes.length(); i++) {
                    String name = attributes.getString(i);
                    tracker.push(format("type[%s]", name));
                    Object oValue = types.get(name);
                    if (oValue instanceof JSONObject || oValue instanceof JSONArray) {
                        throw new JSONValidationException(
                            "document.types cannot contain arrays or objects"
                        );
                    }
                    String value = types.getString(name);
                    Map<String, String> validations = getValidations(name, value);
                    if (!validations.containsKey(TYPE)) {
                        throw new JSONValidationException(
                            "document.types entries must contain a 'type' attribute"
                        );
                    }
                    _userDefinedTypes.put(name, validations);
                    tracker.pop();
                }

            } catch (JSONException e) {
                throw new JSONValidationException(
                    "document.types is not a valid JSON object", e
                );
            }
        }
    } // extractUserDefinedTypes()

    /*
        validate double is in range, as/if defined
     */
    private void validateDouble(String attribute,
                                Object value,
                                Map<String, String> validations)
        throws JSONValidationException {
        Double dblVal = getDouble(attribute, value);

        // min value validation
        Double minVal = getDouble(attribute, validations, MIN);
        if (null != minVal && dblVal < minVal) {
            throw new JSONValidationException(
                format(
                    "%s = %f is out of range, min = %f",
                    attribute,
                    dblVal,
                    minVal
                )
            );
        }

        // max value validation
        Double maxVal = getDouble(attribute, validations, MAX);
        if (null != maxVal && dblVal > maxVal) {
            throw new JSONValidationException(
                format(
                    "%s = %f is out of range, max = %f",
                    attribute,
                    dblVal,
                    maxVal
                )
            );
        }
    } // validateDouble()

    /*
        validate enumerated value - case insensitive
     */
    private void validateEnum(String attribute,
                              Object value,
                              Map<String, String> validations)
        throws JSONValidationException {
        if (value instanceof String) {
            String enumValue = ((String)value).trim().toLowerCase();
            String values = validations.get("values");

            // parse the values and put them into a set
            Set<String> valueSet = new HashSet<>();
            if (null != values) {
                for (String enumEntry : values.split("[,]")) {
                    enumEntry = enumEntry.trim().toLowerCase();
                    if (enumEntry.length() > 0) {
                        valueSet.add(enumEntry);
                    }
                }
            }

            // make sure schema enumeration isn't empty and that value is in it
            if (valueSet.isEmpty()) {
                throw new JSONValidationException(
                    format("enum schema for '%s' has no 'values'", attribute)
                );
            } else if (!valueSet.contains(enumValue)) {
                throw new JSONValidationException(
                    format(
                        "enum '%s' value '%s' is not in enumeration '%s'",
                        attribute,
                        enumValue,
                        values
                    )
                );
            }
        } else {
            throw new JSONValidationException(
                format(
                    "'%s' is an enum and must be of type string; found %s",
                    attribute,
                    value
                )
            );
        }
    } // validateEnum()

    /*
        look up the validation rule using the given key and try to parse
        the resulting value as a double
     */
    private Double getDouble(String attribute,
                             Map<String, String> validations,
                             String key) throws JSONValidationException {
        Double result = null;

        String value = validations.get(key);
        if (null != value) {
            try {
                result = Double.parseDouble(value);
            } catch (NumberFormatException nfe) {
                throw new JSONValidationException(
                    format(
                        "invalid schema : %s value of %s for %s",
                        key, value, attribute
                    ),
                    nfe
                );
            }
        }
        return result;
    } // getDouble()

    /*
        try to obtain a double from the given value
     */
    private Double getDouble(String attribute, Object value)
        throws JSONValidationException {
        Double dblVal = null;
        if (value instanceof Double) {
            dblVal = (Double)value;
        } else if (value instanceof String) {
            try {
                dblVal = Double.parseDouble((String) value);
            } catch (NumberFormatException nfe) {
                dblVal = null;
            }
        } else if (value instanceof Long) {
            dblVal = (double)(Long)value;
        } else if (value instanceof Integer) {
            dblVal = (double)(Integer)value;
        }

        if (null == dblVal) {
            throw new JSONValidationException(
                format(
                    "'%s' with value '%s' is not a valid double",
                    attribute,
                    value
                )
            );
        }

        return dblVal;
    } // getInt()

    /*
        look up the validation rule using the given key and try to parse
        the resulting value as an int
     */
    private Integer getInt(String attribute,
                           Map<String, String> validations,
                           String key) throws JSONValidationException {
        Integer result = null;

        String value = validations.get(key);
        if (null != value) {
            try {
                result = Integer.parseInt(value);
            } catch (NumberFormatException nfe) {
                throw new JSONValidationException(
                    format(
                        "invalid schema : %s value of %s for %s",
                        key, value, attribute
                    ),
                    nfe
                );
            }
        }

        return result;
    } // getInt()

    /*
        try to obtain an int from the given value
     */
    private Integer getInt(String attribute, Object value)
        throws JSONValidationException {
        Integer intVal = null;
        if (value instanceof Integer) {
            intVal = (Integer)value;
        } else if (value instanceof String) {
            try {
                intVal = Integer.parseInt((String) value);
            } catch (NumberFormatException nfe) {
                intVal = null;
            }
        }

        if (null == intVal) {
            throw new JSONValidationException(
                format(
                    "'%s' with value '%s' is not a valid integer",
                    attribute,
                    value
                )
            );
        }

        return intVal;
    } // getInt()

    /*
        validate that the given value satisfies the given validation rules for an int
     */
    private void validateInt(String attribute,
                             Object value,
                             Map<String, String> validations)
        throws JSONValidationException {

        Integer intVal = getInt(attribute, value);

        // check min value
        Integer minVal = getInt(attribute, validations, MIN);
        if (null != minVal && intVal < minVal) {
            throw new JSONValidationException(
                format(
                    "%s = %d is out of range, min = %d",
                    attribute,
                    intVal,
                    minVal
                )
            );
        }

        // check max value
        Integer maxVal = getInt(attribute, validations, MAX);
        if (null != maxVal && intVal > maxVal) {
            throw new JSONValidationException(
                format(
                    "%s = %d is out of range, max = %d",
                    attribute,
                    intVal,
                    maxVal
                )
            );
        }

    } // validateInt()

    /*
        look up the validation rule using the given key and try to parse
        the resulting value as a long
     */
    private Long getLong(String attribute,
                         Map<String, String> validations,
                         String key) throws JSONValidationException {
        Long result = null;

        String value = validations.get(key);
        if (null != value) {
            try {
                result = Long.parseLong(value);
            } catch (NumberFormatException nfe) {
                throw new JSONValidationException(
                    format(
                        "invalid schema : %s value of %s for %s",
                        key, value, attribute
                    ),
                    nfe
                );
            }
        }

        return result;
    } // getLong()

    /*
        try to obtain a long from the given value
     */
    private Long getLong(String attribute, Object value)
        throws JSONValidationException {
        Long longVal = null;
        if (value instanceof Long) {
            longVal = (Long)value;
        } else if (value instanceof Integer) {
            Integer intVal = getInt(attribute, value);
            longVal = Long.valueOf(intVal);
        } else if (value instanceof String) {
            try {
                longVal = Long.parseLong((String) value);
            } catch (NumberFormatException nfe) {
                longVal = null;
            }
        }

        if (null == longVal) {
            throw new JSONValidationException(
                format(
                    "'%s' with value '%s' is not a valid long number",
                    attribute,
                    value
                )
            );
        }

        return longVal;
    } // getLong()

    /*
        validate that the given value satisfies the given validation rules for a long
     */
    private void validateLong(String attribute,
                              Object value,
                              Map<String, String> validations)
        throws JSONValidationException {

        Long longVal = getLong(attribute, value);

        // check min
        Long minVal = getLong(attribute, validations, MIN);
        if (null != minVal && longVal < minVal) {
            throw new JSONValidationException(
                format(
                    "%s = %,d is out of range, min = %,d",
                    attribute,
                    longVal,
                    minVal
                )
            );
        }

        // check max
        Long maxVal = getLong(attribute, validations, MAX);
        if (null != maxVal && longVal > maxVal) {
            throw new JSONValidationException(
                format(
                    "%s = %,d is out of range, max = %,d",
                    attribute,
                    longVal,
                    maxVal
                )
            );
        }
    } // validateLong()

    /*
        validate that the given value is a string
     */
    private void validateString(String attribute,
                                Object value,
                                Map<String, String> validations)
        throws JSONValidationException {
        if (!(value instanceof String)) {
            throw new JSONValidationException(
                format(
                    "%s with value %s is not of type string",
                    attribute,
                    value
                )
            );
        }

        String strVal = (String)value;

        if (null != validations && validations.size() > 0) {
            for (Map.Entry<String, String> entry : validations.entrySet()) {
                switch(entry.getKey().toLowerCase()) {

                    case MIN :
                        Integer minAttr = getInt(MIN, entry.getValue());
                        if (strVal.length() < minAttr) {
                            throw new JSONValidationException(
                                format(
                                    "attribute %s with value %s is below minimum length of %d",
                                    attribute,
                                    strVal,
                                    minAttr
                                )
                            );
                        }
                        break;

                    case MAX :
                        Integer maxAttr = getInt(MAX, entry.getValue());
                        if (strVal.length() > maxAttr) {
                            throw new JSONValidationException(
                                format(
                                    "attribute %s with value %s is above maximum length of %d",
                                    attribute,
                                    strVal,
                                    maxAttr
                                )
                            );
                        }
                        break;

                    case REGEX :
                        if (!strVal.matches(entry.getValue())) {
                            throw new JSONValidationException(
                                format(
                                    "attribute %s with value %s does not match regex %s",
                                    attribute,
                                    strVal,
                                    entry.getValue()
                                )
                            );
                        }
                        break;

                }
            }
        }
    } // validateString()

    /*
        validate that the given value is a valid uuid
     */
    private void validateUuid(String attribute, Object value)
        throws JSONValidationException {
        if (value instanceof String) {
            Matcher matcher = UUID_PATTERN.matcher((String)value);
            if (!matcher.matches()) {
                throw new JSONValidationException(
                    format(
                        "'%s' with value '%s' is not a valid uuid",
                        attribute,
                        value
                    )
                );
            }
        } else {
            throw new JSONValidationException(
                format(
                    "%s with value %s is not of type uuid",
                    attribute,
                    value
                )
            );
        }
    } // validateUuid()

    /*
        validate that the given value is of the expected type and that it
        satisfies any other validation rules specified
     */
    private void validateValue(String attribute, Object value, String validation)
        throws JSONValidationException {

        Map<String, String> validations = getValidations(attribute, validation);
        String type = validations.get(TYPE);

        if (null != type) {
            if (!NATIVE_TYPES.contains(type)) {
                type = addUserType(attribute, type, validations);
            }

            switch (type) {

                case "int" :
                    validateInt(attribute, value, validations);
                    break;

                case "long" :
                    validateLong(attribute, value, validations);
                    break;

                case "date" :
                    validateDate(attribute, value, validations);
                    break;

                case "double" :
                    validateDouble(attribute, value, validations);
                    break;

                case "enum" :
                    validateEnum(attribute, value, validations);
                    break;

                case "bool" :
                    validateBoolean(attribute, value);
                    break;

                case "uuid" :
                    validateUuid(attribute, value);
                    break;

                case "string" :
                    validateString(attribute, value, validations);
                    break;

                default :
                    throw new JSONValidationException(
                        format("unsupported attribute type '%s'", type)
                    );
            } // case
        } else {
            throw new JSONValidationException("required attribute 'type' is missing");
        }
    } // validateValue()

} // class validateAttribute
