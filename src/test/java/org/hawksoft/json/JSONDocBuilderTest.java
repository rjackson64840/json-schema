/*
The MIT License (MIT)

Copyright (c) 2013-2017 Russ Jackson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

 */
package org.hawksoft.json;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created at 1:37 PM on 2017-04-26
 *
 * @author Russ Jackson
 */
public class JSONDocBuilderTest {

    public JSONDocBuilderTest() {
        // default constructor
    } // constructor

    @Test
    public void testBadParams() {
        try {
            JSONDocBuilder.startDocument("", "idX", "versionX");
            fail("expected exception");
        } catch(IllegalArgumentException e) {
            // expected
        }
        try {
            JSONDocBuilder.startDocument(" ", "idX", "versionX");
            fail("expected exception");
        } catch(IllegalArgumentException e) {
            // expected
        }
        try {
            JSONDocBuilder.startDocument(null, "idX", "versionX");
            fail("expected exception");
        } catch(IllegalArgumentException e) {
            // expected
        }
        try {
            JSONDocBuilder.startDocument("ns4", "", "versionX");
            fail("expected exception");
        } catch(IllegalArgumentException e) {
            // expected
        }
        try {
            JSONDocBuilder.startDocument("ns4", " ", "versionX");
            fail("expected exception");
        } catch(IllegalArgumentException e) {
            // expected
        }
        try {
            JSONDocBuilder.startDocument("ns3", null, "versionX");
            fail("expected exception");
        } catch(IllegalArgumentException e) {
            // expected
        }
        try {
            JSONDocBuilder.startDocument("ns2", "idX", "");
            fail("expected exception");
        } catch(IllegalArgumentException e) {
            // expected
        }
        try {
            JSONDocBuilder.startDocument("ns2", "idX", " ");
            fail("expected exception");
        } catch(IllegalArgumentException e) {
            // expected
        }
        try {
            JSONDocBuilder.startDocument("ns1", "idX", null);
            fail("expected exception");
        } catch(IllegalArgumentException e) {
            // expected
        }
    } // testBadParams()

    @Test
    public void testStartDocument() {
        JSONObject json = JSONDocBuilder.startDocument("ns1", "id2", "v3");
        try {
            JSONObject doc = json.getJSONObject("document");
            assertEquals("instance", doc.getString("type"));
            assertEquals("ns1", doc.getString("namespace"));
            assertEquals("id2", doc.getString("id"));
            assertEquals("v3", doc.getString("version"));
        } catch (JSONException e) {
            fail(e.getMessage());
        }
    } // testStartDocument()

} // class JSONDocBuilderTest
