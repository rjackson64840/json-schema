/*
The MIT License (MIT)

Copyright (c) 2013-2017 Russ Jackson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

 */
package org.hawksoft.json;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import static org.junit.Assert.*;

/**
 * Created at 6:35 PM on 2013-12-20
 *
 * @author Russ Jackson (last updated by $Author: arjackson $)
 * @version $Revision: 24 $, $Date: 2014-03-02 13:14:59 -0600 (Sun, 02 Mar 2014) $
 */
public class JSONSchemaRepositoryTest {

    private static final Logger _log =
        LoggerFactory.getLogger(JSONSchemaRepositoryTest.class);

    public JSONSchemaRepositoryTest() {
    } // constructor

    @Test
    public void testRepository() {
        try {
            JSONSchemaRepository repo = new JSONSchemaRepository(
                "/org/hawksoft/json"
            );
            JSONObject schema = repo.getSchema("org.hawksoft", "test", "1.0");
            assertNotNull(schema);

            schema = repo.getSchema("nope", "soap", "hope");
            assertNull(schema);

            Map<String, String> schemas = repo.getAllSchemas();
            assertNotNull(schemas);
            assertEquals(1, schemas.size());

            Map<String, String> examples = repo.getAllExamples();
            assertNotNull(examples);
            assertEquals(1, examples.size());

            Map<String, String> ex2 = repo.getAllExamples();
            assertSame(examples, ex2);
        } catch (JSONValidationException e) {
            fail(e.getMessage());
        }

        try {
            new JSONSchemaRepository(
                "/org/hawksoft/json/bad"
            );
            fail("exception expected loading schema repository");
        } catch (JSONValidationException e) {
            // expected
        }
    } // testRepository()

    @Test
    public void testExample() {
        try {
            JSONSchemaRepository repo = new JSONSchemaRepository(
                "/org/hawksoft/json"
            );

            JSONObject example = repo.getExample("org.hawksoft", "test", "1.0");
            assertTrue(example.getBoolean("test-boolean"));
            assertFalse(example.getBoolean("test-false"));
        } catch (JSONValidationException | JSONException e) {
            fail(e.getMessage());
        }

    } // testExample()

    @Test
    public void testAttributeMissingEquals() {
        try {
            JSONSchemaRepository repo = new JSONSchemaRepository(
                "/org/hawksoft/json/bad-missing-equals"
            );

            repo.getExample("org.hawksoft", "test", "1.0");
            fail("expected exception for bad attribute");
        } catch (JSONValidationException e) {
            assertEquals(
                "invalid schema attribute entry 'type=date;desc end date for iteration;example=2013-10-24;'",
                e.getMessage()
            );
        }

    } // testAttributeMissingEquals()


} // class JSONSchemaRepositoryTest
