/*
The MIT License (MIT)

Copyright (c) 2013-2017 Russ Jackson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

 */
package org.hawksoft.json;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CyclicBarrier;

import static java.lang.String.format;
import static org.junit.Assert.*;

/**
 * Created at 2:22 PM on 2013-12-16
 *
 * @author Russ Jackson (last updated by $Author: arjackson $)
 * @version $Revision: 42 $, $Date: 2014-06-21 22:02:17 -0500 (Sat, 21 Jun 2014) $
 */
public class JSONSchemaValidatorTest {

    private static final Logger _log =
        LoggerFactory.getLogger(JSONSchemaValidatorTest.class);

    public JSONSchemaValidatorTest() {
    } // constructor

    private JSONObject getValidDocument() throws JSONException {
        return new JSONObject(
            "{document:{type=schema,namespace=org.hawksoft,id=test,version=1.0}}"
        );
    } // getValidDocument()

    private String loadDocument(String docName) throws JSONException, IOException {
        InputStream is = this.getClass().getResourceAsStream(docName);
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        while (null != (line = reader.readLine())) {
            sb.append(line);
        }
        return sb.toString();
    } // loadDocument()

    @Test
    public void testDocument() {
        String document = null;
        try {
            document = loadDocument("test-doc-good.json");
        } catch (JSONException | IOException e) {
            fail(e.getMessage());
        }

        JSONSchemaRepository repo = null;
        try {
            repo = new JSONSchemaRepository(
                "/org/hawksoft/json"
            );
        } catch (JSONValidationException e) {
            fail(e.getMessage());
        }

        JSONObject json = null;
        try {
            json = new JSONObject(document);
        } catch (JSONException e) {
            fail(e.getMessage());
        }

        // happy path
        try {
            // validate string input
            JSONSchemaValidator.validateDocument(document, repo);
            // validate JSONObject input
            JSONSchemaValidator.validateDocument(json, repo);

            // validate input stream input
            InputStream is = this.getClass().getResourceAsStream("test-doc-good.json");
            JSONSchemaValidator.validateDocument(is, repo);
        } catch (JSONValidationException e) {
            fail(e.getMessage());
        }

        // wrong document type
        try {
            json = getValidDocument();
            json.getJSONObject("document").put("type", "schema");
            JSONSchemaValidator.validateDocument(json, repo);
            fail("JSONValidationException expected");
        } catch (JSONException e) {
            fail(e.getMessage());
        } catch (JSONValidationException e) {
            //expected
            assertEquals("value 'schema' for property 'type' does not match expected value 'instance'", e.getMessage());
        }

        String schema = "{document:{type=schema,model=1311.0,namespace=org.hawksoft,id=test,version=1.0}}";
        String doc;

        // wrong document type via string
        try {
            doc = "{document:{type=schema,namespace=org.hawksoft,id=test,version=1.0}}";
            JSONSchemaValidator.validateDocument(doc, schema);
            fail("JSONValidationException expected");
        } catch (JSONValidationException e) {
            //expected
        }

        // invalid json
        try {
            doc = "[{document:{type=schema,namespace=org.hawksoft,id=test,version=1.0}}";
            JSONSchemaValidator.validateDocument(doc, schema);
            fail("JSONValidationException expected");
        } catch (JSONValidationException e) {
            //expected
        }

        // missing namespace
        try {
            doc = "{document:{type=instance,namespace='',id=test,version=1.0}}";
            JSONSchemaValidator.validateDocument(doc, schema);
            fail("JSONValidationException expected");
        } catch (JSONValidationException e) {
            //expected
            assertEquals("required property 'namespace' is blank", e.getMessage());
        }

        // missing id
        try {
            doc = "{document:{type=instance,namespace=org.hawksoft,id='',version=1.0}}";
            JSONSchemaValidator.validateDocument(doc, schema);
            fail("JSONValidationException expected");
        } catch (JSONValidationException e) {
            //expected
            assertEquals("required property 'id' is blank", e.getMessage());
        }

        // missing version
        try {
            doc = "{document:{type=instance,namespace=org.hawksoft,id=test}}";
            JSONSchemaValidator.validateDocument(doc, schema);
            fail("JSONValidationException expected");
        } catch (JSONValidationException e) {
            //expected
            assertEquals("error accessing string property 'version'", e.getMessage());
        }

        // missing document child
        try {
            doc = "{data:{type=instance,namespace=org.hawksoft,id=test}}";
            JSONSchemaValidator.validateDocument(doc, schema);
            fail("JSONValidationException expected");
        } catch (JSONValidationException e) {
            //expected
            assertEquals("required child object 'document' is missing", e.getMessage());
        }

//        // test input stream - good
//        try {
//            InputStream is = this.getClass().getResourceAsStream(
//                "/org/hawksoft/json/test-doc-good.json"
//            );
//            JSONSchemaValidator.validateDocument(is, null);
//        } catch (Exception e) {
//            fail(e.getMessage());
//        }
//
//        // test input stream - bad
//        try {
//            InputStream is = this.getClass().getResourceAsStream(
//                "/org/hawksoft/json/test-doc-bad.json"
//            );
//            JSONSchemaValidator.validateDocument(is, null);
//            fail("JSONValidationException expected");
//        } catch (JSONValidationException e) {
//            // expected
//        }

        // custom input stream to throw IOException
        try {
            JSONSchemaValidator.validateDocument(new InputStream() {

                @Override
                public int read() throws IOException {
                    throw new IOException();
                }
            }, repo);
            fail("JSONValidationException expected");
        } catch (JSONValidationException e) {
            // expected
            assertEquals("java.io.IOException", e.getMessage());
        }

    } // testDocument()

    private JSONObject getValidSchema() throws JSONException {
        JSONObject json = new JSONObject(
            "{document:{type=schema,model=1311.0,namespace=org.hawksoft,id=test,version=1.0}}"
        );
        JSONObject schema = new JSONObject();
        json.put("schema", schema);

        JSONObject element = new JSONObject();
        schema.put("element", element);

        element.put("type", "int");
        element.put("description", "element description");
        element.put("example", 14);

        return json;
    } // getValidSchema()

    @Test
    public void testSchema() {
        JSONObject json = null;

        // happy path
        try {
            json = getValidSchema();
            JSONSchemaValidator.validateSchema(json);
        } catch (JSONException | JSONValidationException e) {
            fail(e.getMessage());
        }

        // wrong document type
        try {
            json = getValidSchema();
            json.getJSONObject("document").put("type", "instance");
            JSONSchemaValidator.validateSchema(json);
            fail("JSONValidationException expected");
        } catch (JSONException e) {
            fail(e.getMessage());
        } catch (JSONValidationException e) {
            //expected
            assertEquals("value 'instance' for property 'type' does not match expected value 'schema'", e.getMessage());
        }

        // unsupported model
        try {
            json = getValidSchema();
            json.getJSONObject("document").put("model", 1.0);
            JSONSchemaValidator.validateSchema(json);
            fail("JSONValidationException expected");
        } catch (JSONException e) {
            fail(e.getMessage());
        } catch (JSONValidationException e) {
            //expected
            assertEquals("model '1.0' not supported", e.getMessage());
        }

        // missing model
        try {
            json = getValidSchema();
            json.getJSONObject("document").remove("model");
            JSONSchemaValidator.validateSchema(json);
            fail("JSONValidationException expected");
        } catch (JSONException e) {
            fail(e.getMessage());
        } catch (JSONValidationException e) {
            //expected
            assertEquals("error accessing string property 'model'", e.getMessage());
        }

        // missing namespace
        try {
            json = getValidSchema();
            json.getJSONObject("document").remove("namespace");
            JSONSchemaValidator.validateSchema(json);
            fail("JSONValidationException expected");
        } catch (JSONException e) {
            fail(e.getMessage());
        } catch (JSONValidationException e) {
            //expected
            assertEquals("error accessing string property 'namespace'", e.getMessage());
        }

        // missing id
        try {
            json = getValidSchema();
            json.getJSONObject("document").remove("id");
            JSONSchemaValidator.validateSchema(json);
            fail("JSONValidationException expected");
        } catch (JSONException e) {
            fail(e.getMessage());
        } catch (JSONValidationException e) {
            //expected
            assertEquals("error accessing string property 'id'", e.getMessage());
        }

        // missing version
        try {
            json = getValidSchema();
            json.getJSONObject("document").remove("version");
            JSONSchemaValidator.validateSchema(json);
            fail("JSONValidationException expected");
        } catch (JSONException e) {
            fail(e.getMessage());
        } catch (JSONValidationException e) {
            //expected
            assertEquals("error accessing string property 'version'", e.getMessage());
        }

        // missing document child
        try {
            json = new JSONObject(
                "{data:{type=schema,model=1311.0,namespace=org.hawksoft,id=test,version=1.0}}"
            );
            json.put("schema", new JSONObject());
            JSONSchemaValidator.validateSchema(json);
            fail("JSONValidationException expected");
        } catch (JSONException e) {
            fail(e.getMessage());
        } catch (JSONValidationException e) {
            //expected
            assertEquals("required child object 'document' is missing", e.getMessage());
        }

    } // testSchema()

    @Test
    public void testArray() {
        String schema =
            "{document:{type=schema,model=1311.0,namespace=org.hawksoft,id=test,version=1.0}," +
            "test-array:['type=int;desc=tbd;example=tbd']}";
        String doc = "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}}";

        try {
            // missing required array
            JSONSchemaValidator.validateDocument(doc, schema);
            fail("exception expected for missing array");
        } catch (JSONValidationException e) {
            // expected
            assertEquals("required array 'test-array' is missing", e.getMessage());
        }

        // make the array optional
        schema =
            "{document:{type=schema,model=1311.0,namespace=org.hawksoft,id=test,version=1.0}," +
            "'test-array?':['type=int;desc=tbd;example=tbd']}";
        try {
            JSONSchemaValidator.validateDocument(doc, schema);
        } catch (JSONValidationException e) {
            fail(e.getMessage());
        }

        schema =
            "{document:{type=schema,model=1311.0,namespace=org.hawksoft,id=test,version=1.0}," +
            "test-array:['type=int;desc=tbd;example=tbd']}";
        doc = "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
              "'test-array':[14]}";
        try {
            JSONSchemaValidator.validateDocument(doc, schema);
        } catch (JSONValidationException e) {
            fail(e.getMessage());
        }

    } // testArray()

    @Test
    public void testArrayOfArrays() {
        String schema =
            "{document:{type=schema,model=1311.0,namespace=org.hawksoft,id=test,version=1.0}," +
            "nested-array:[['type=int;desc=tbd;example=tbd']]}";
        String doc = "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
                     "'nested-array':[[14,13,12],[11]]}";
        try {
            JSONSchemaValidator.validateDocument(doc, schema);
        } catch (JSONValidationException e) {
            fail(e.getMessage());
        }
    } // testArrayOfArrays()

    @Test
    public void testObject() {
        String schema =
            "{document:{type=schema,model=1311.0,namespace=org.hawksoft,id=test,version=1.0}," +
            "test-obj:{child:'type=int;desc=tbd;example=tbd'}}";
        String doc = "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}}";

        try {
            // missing required obj
            JSONSchemaValidator.validateDocument(doc, schema);
            fail("exception expected for missing object");
        } catch (JSONValidationException e) {
            // expected
            assertEquals("required object 'test-obj' is missing", e.getMessage());
        }

        // make the object optional
        schema =
            "{document:{type=schema,model=1311.0,namespace=org.hawksoft,id=test,version=1.0}," +
            "'test-obj?':{child:'type=int;desc=tbd;example=tbd'}}";
        try {
            JSONSchemaValidator.validateDocument(doc, schema);
        } catch (JSONValidationException e) {
            fail(e.getMessage());
        }

    } // testObject()

    @Test
    public void testUnknownAttributeType() {
        String schema =
            "{document:{type=schema,model=1311.0,namespace=org.hawksoft,id=test,version=1.0}," +
            "test-field:'type=what;desc=tbd;example=tbd'}";
        String doc =
            "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
            "test-field:1}";

        try {
            // missing required array
            JSONSchemaValidator.validateDocument(doc, schema);
            fail("exception expected for unknown attribute type");
        } catch (JSONValidationException e) {
            // expected
            assertEquals(
                "user defined type 'what' referenced by attribute 'test-field' not found",
                e.getMessage()
            );
        }
    } // testUnknownAttributeType()

    @Test
    public void testMissingTypeAttribute() {
        String schema =
            "{document:{type=schema,model=1311.0,namespace=org.hawksoft,id=test,version=1.0}," +
            "test-field:'desc=tbd;example=tbd'}";
        String doc =
            "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
            "test-field:1}";

        try {
            JSONSchemaValidator.validateDocument(doc, schema);
            fail("exception expected for unknown attribute type");
        } catch (JSONValidationException e) {
            // expected
            assertEquals("required attribute 'type' is missing", e.getMessage());
        }
    } // testMissingTypeAttribute()

    @Test
    public void testOptionalWithNull() {
        String schema =
            "{document:{type=schema,model=1311.0,namespace=org.hawksoft,id=test,version=1.0}," +
                "test-field?:'type=int;desc=tbd;example=tbd'}";
        String doc =
            "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
                "test-field:null}";

        try {
            JSONSchemaValidator.validateDocument(doc, schema);
        } catch (JSONValidationException e) {
            fail(e.getMessage());
        }
    } // testOptionalWithNull()

    public void testNull() {

    } // testNull()

    @Test
    public void testIncompleteAttribute() {
        String schema =
            "{document:{type=schema,model=1311.0,namespace=org.hawksoft,id=test,version=1.0}," +
            "test-field:'type=int;desc=;example=tbd'}";
        String doc =
            "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
            "test-field:4}";

        try {
            JSONSchemaValidator.validateDocument(doc, schema);
            fail("exception expected for incomplete schema attribute");
        } catch (JSONValidationException e) {
            // expected
            assertEquals("invalid schema attribute entry 'desc='", e.getMessage());
        }
    } // testIncompleteAttribute()

    @Test
    public void testAttributeMissingEquals() {
        String schema =
            "{document:{type=schema,model=1311.0,namespace=org.hawksoft,id=test,version=1.0}," +
            "test-field:'type=int;desc some desc;example=tbd'}";
        String doc =
            "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
            "test-field:4}";

        try {
            JSONSchemaValidator.validateDocument(doc, schema);
            fail("exception expected for incomplete schema attribute");
        } catch (JSONValidationException e) {
            // expected
            assertEquals("invalid schema attribute entry 'desc some desc'", e.getMessage());
        }
    } // testAttributeMissingEquals()

    @Test
    public void testUnkownAttributeTypeInsideArray() {
        String schema =
            "{document:{type=schema,model=1311.0,namespace=org.hawksoft,id=test,version=1.0}," +
            "test-array:['type=what;desc=tbd;example=tbd']}";
        String doc =
            "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
            "test-array:[1]}";

        try {
            // missing required array
            JSONSchemaValidator.validateDocument(doc, schema);
            fail("exception expected for unknown attribute inside array");
        } catch (JSONValidationException e) {
            // expected
            assertEquals(
                "user defined type 'what' referenced by attribute '' not found",
                e.getMessage()
            );
        }
    } // testUnkownAttributeTypeInsideArray()

    @Test
    public void testMissingTypeAttributeInsideArray() {
        String schema =
            "{document:{type=schema,model=1311.0,namespace=org.hawksoft,id=test,version=1.0}," +
            "test-array:['desc=tbd;example=tbd']}";
        String doc =
            "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
            "test-array:[1]}";

        try {
            JSONSchemaValidator.validateDocument(doc, schema);
            fail("exception expected for unknown attribute type");
        } catch (JSONValidationException e) {
            // expected
            assertEquals("required attribute 'type' is missing", e.getMessage());
        }
    } // testMissingTypeAttributeInsideArray()

    @Test
    public void testIncompleteAttributeInsideArray() {
        String schema =
            "{document:{type=schema,model=1311.0,namespace=org.hawksoft,id=test,version=1.0}," +
            "test-array:['type=int;desc=;example=tbd']}";
        String doc =
            "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
            "test-array:[4]}";

        try {
            JSONSchemaValidator.validateDocument(doc, schema);
            fail("exception expected for incomplete schema attribute inside array");
        } catch (JSONValidationException e) {
            // expected
            assertEquals("invalid schema attribute entry 'desc='", e.getMessage());
        }
    } // testIncompleteAttributeInsideArray()

    @Test
    public void testInvalidArraySchema() {
        String schema =
            "{document:{type=schema,model=1311.0,namespace=org.hawksoft,id=test,version=1.0}," +
            "test-array:['type=int;desc=tbd;example=tbd','type=int;desc=tbd;example=tbd']}";
        String doc =
            "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
            "test-array:[1]}";

        try {
            JSONSchemaValidator.validateDocument(doc, schema);
            fail("exception expected for duplicate array element schema");
        } catch (JSONValidationException e) {
            // expected
            assertEquals(
                "array element schema must contain one and only one entry",
                e.getMessage()
            );
        }
    } // testInvalidArraySchema()

    @Test
    public void testMissingSchema() {
        JSONSchemaRepository repo = null;
        try {
            repo = new JSONSchemaRepository(
                "/org/hawksoft/json"
            );
        } catch (JSONValidationException e) {
            fail(e.getMessage());
        }

        String doc =
            "{document:{type=instance,namespace=org.hawksoft,id=test,version=2.0}," +
            "test-array:[1]}";

        try {
            JSONSchemaValidator.validateDocument(doc, repo);
            fail("exception expected for missing schema");
        } catch (JSONValidationException e) {
            // expected
            assertEquals("schema missing for 'org.hawksoft-test-2.0'", e.getMessage());
        }
    } // testMissingSchema()

    @Test
    public void testExtraneousAttributes() {
        String schema =
            "{document:{type=schema,model=1311.0,namespace=org.hawksoft,id=test,version=1.0}," +
            "test-array:['type=int;desc=tbd;example=tbd']}";
        String doc =
            "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
            "test-array:[1],extra1:bad,extra2:even worse}";

        try {
            JSONSchemaValidator.validateDocument(doc, schema);
            fail("exception expected for extraneous document attribute(s)");
        } catch (JSONValidationException e) {
            // expected
            assertEquals(
                "the document contains the following extraneous attribute(s): extra1, extra2",
                e.getMessage()
            );
        }
    } // testExtraneousAttributes()

    @Test
    public void testInteger() {
        String schema =
            "{document:{type=schema,model=1311.0,namespace=org.hawksoft,id=test,version=1.0}," +
            "test-int:'type=int;desc=tbd;example=tbd'}";
        String doc =
            "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
            "test-int:1}";

        try {
            JSONSchemaValidator.validateDocument(doc, schema);
        } catch (JSONValidationException e) {
            fail(e.getMessage());
        }

        doc = "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
              "test-int:a}";

        try {
            JSONSchemaValidator.validateDocument(doc, schema);
            fail("exception invalid int");
        } catch (JSONValidationException e) {
            // expected
            assertEquals(
                "'test-int' with value 'a' is not a valid integer",
                e.getMessage()
            );
        }
    } // testInteger()

    @Test
    public void testIntegerRanges() {
        String schema =
            "{document:{type=schema,model=1311.0,namespace=org.hawksoft,id=test,version=1.0}," +
            "test-int:'type=int;min=-1;max=20;desc=tbd;example=tbd'}";
        String doc =
            "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
            "test-int:-10}";

        Map<Integer, Boolean> valueResultMap = new HashMap<>();
        valueResultMap.put(-2, false);
        valueResultMap.put(-1, true);
        valueResultMap.put(0, true);
        valueResultMap.put(19, true);
        valueResultMap.put(20, true);
        valueResultMap.put(21, false);

        JSONObject oDoc = null;
        JSONObject oSchema = null;
        try {
            oDoc = new JSONObject(doc);
            oSchema = new JSONObject(schema);
        } catch (JSONException e) {
            fail(e.getMessage());
        }

        for (Map.Entry<Integer, Boolean> entry : valueResultMap.entrySet()) {
            try {
                oDoc.put("test-int", entry.getKey());
                try {
                    JSONSchemaValidator.validateDocument(oDoc, oSchema);
                    if (!entry.getValue()) {
                        fail("expected int out of range exception");
                    }
                } catch (JSONValidationException e) {
                    if (entry.getValue()) {
                        // unexpected
                        fail(e.getMessage());
                    } else {
                        // expected
                        assertTrue(e.getMessage().contains("out of range"));
                    }
                }
            } catch (JSONException e) {
                fail(e.getMessage());
            }
        }
    } // testIntegerRanges()

    @Test
    public void testLongRanges() {
        String schema =
            "{document:{type=schema,model=1311.0,namespace=org.hawksoft,id=test,version=1.0}," +
            "test-long:'type=long;min=-1;max=20;desc=tbd;example=tbd'}";
        String doc =
            "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
            "test-long:-10}";

        Map<Long, Boolean> valueResultMap = new HashMap<>();
        valueResultMap.put(-2L, false);
        valueResultMap.put(-1L, true);
        valueResultMap.put(0L, true);
        valueResultMap.put(19L, true);
        valueResultMap.put(20L, true);
        valueResultMap.put(21L, false);

        JSONObject oDoc = null;
        JSONObject oSchema = null;
        try {
            oDoc = new JSONObject(doc);
            oSchema = new JSONObject(schema);
        } catch (JSONException e) {
            fail(e.getMessage());
        }

        for (Map.Entry<Long, Boolean> entry : valueResultMap.entrySet()) {
            try {
                oDoc.put("test-long", entry.getKey());
                try {
                    JSONSchemaValidator.validateDocument(oDoc, oSchema);
                    if (!entry.getValue()) {
                        fail("expected long value out of range exception");
                    }
                } catch (JSONValidationException e) {
                    if (entry.getValue()) {
                        // unexpected
                        fail(e.getMessage());
                    } else {
                        // expected
                        assertTrue(e.getMessage().contains("out of range"));
                    }
                }
            } catch (JSONException e) {
                fail(e.getMessage());
            }
        }
    } // testLongRanges()

    @Test
    public void testString() {
        String schema =
            "{document:{type=schema,model=1311.0,namespace=org.hawksoft,id=test,version=1.0}," +
            "test:'type=string;desc=tbd;example=tbd'}";
        String doc =
            "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
            "test:'feliz navidad'}";

        try {
            JSONSchemaValidator.validateDocument(doc, schema);
        } catch (JSONValidationException e) {
            fail(e.getMessage());
        }

        doc = "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
              "test:100}";

        try {
            JSONSchemaValidator.validateDocument(doc, schema);
            fail("exception invalid string");
        } catch (JSONValidationException e) {
            // expected
            assertEquals(
                "test with value 100 is not of type string",
                e.getMessage()
            );
        }
    } // testString()

    @Test
    public void testStringMin() {
        String schema =
            "{document:{type=schema,model=1311.0,namespace=org.hawksoft,id=test,version=1.0}," +
            "test:'type=string;min=10;desc=tbd;example=tbd'}";
        String doc =
            "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
            "test:'1234567890'}";

        try {
            JSONSchemaValidator.validateDocument(doc, schema);
        } catch (JSONValidationException e) {
            fail(e.getMessage());
        }

        doc = "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
              "test:'123456789'}";

        try {
            JSONSchemaValidator.validateDocument(doc, schema);
            fail("expected exception for string min violation");
        } catch (JSONValidationException e) {
            // expected
        }

    } // testStringMin()

    @Test
    public void testStringMax() {

        String schema =
            "{document:{type=schema,model=1311.0,namespace=org.hawksoft,id=test,version=1.0}," +
            "test:'type=string;max=10;desc=tbd;example=tbd'}";
        String doc =
            "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
            "test:'1234567890'}";

        try {
            JSONSchemaValidator.validateDocument(doc, schema);
        } catch (JSONValidationException e) {
            fail(e.getMessage());
        }

        doc = "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
              "test:'1234567890A'}";

        try {
            JSONSchemaValidator.validateDocument(doc, schema);
            fail("expected exception for string max violation");
        } catch (JSONValidationException e) {
            // expected
        }

    } // testStringMax()

    @Test
    public void testStringRegex() {
        String schema =
            "{document:{type=schema,model=1311.0,namespace=org.hawksoft,id=test,version=1.0}," +
            "test:'type=string;regex=^[A].*;desc=tbd;example=tbd'}";
        String doc =
            "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
            "test:'AbcdE3f'}";

        try {
            JSONSchemaValidator.validateDocument(doc, schema);
        } catch (JSONValidationException e) {
            fail(e.getMessage());
        }

        doc = "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
              "test:'B1234567890A'}";

        try {
            JSONSchemaValidator.validateDocument(doc, schema);
            fail("expected exception for string regex violation");
        } catch (JSONValidationException e) {
            // expected
        }

    } // testStringRegex()

    @Test
    public void testBoolean() {
        String schema =
            "{document:{type=schema,model=1311.0,namespace=org.hawksoft,id=test,version=1.0}," +
            "test:'type=bool;desc=tbd;example=tbd'}";
        String doc =
            "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
            "test:true}";

        try {
            JSONSchemaValidator.validateDocument(doc, schema);
            doc = "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
                  "test:false}";
            JSONSchemaValidator.validateDocument(doc, schema);
        } catch (JSONValidationException e) {
            fail(e.getMessage());
        }

        doc = "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
              "test:100}";

        try {
            JSONSchemaValidator.validateDocument(doc, schema);
            fail("exception invalid boolean");
        } catch (JSONValidationException e) {
            // expected
            assertEquals(
                "test with value 100 is not of type bool (boolean)",
                e.getMessage()
            );
        }
    } // testBoolean()

    @Test
    public void testDate() {
        String schema =
            "{document:{type=schema,model=1311.0,namespace=org.hawksoft,id=test,version=1.0}," +
            "test:'type=date;desc=tbd;example=tbd'}";
        String doc =
            "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
            "test:2013-12-31}";

        try {
            JSONSchemaValidator.validateDocument(doc, schema);
        } catch (JSONValidationException e) {
            fail(e.getMessage());
        }

        try {
            doc = "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
                  "test:'12/31/2013'}";
            JSONSchemaValidator.validateDocument(doc, schema);
            fail("exception expected for bad date");
        } catch (JSONValidationException e) {
            // expected
            assertTrue(e.getMessage().contains("does not match required format"));
        }
    } // testDate()

    @Test
    public void testDateRanges() {
        String schema =
            "{document:{type=schema,model=1311.0,namespace=org.hawksoft,id=test,version=1.0}," +
            "test:\"type=date;min=2013-12-01;max=2014-01-31;desc=tbd;example=tbd\"}";
        String doc =
            "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
            "test:'2013-12-31'}";

        try {
            JSONSchemaValidator.validateDocument(doc, schema);

            doc = "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
                  "test:'2013-12-01'}";
            JSONSchemaValidator.validateDocument(doc, schema);

            doc = "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
                  "test:'2014-01-01'}";
            JSONSchemaValidator.validateDocument(doc, schema);

            doc = "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
                  "test:'2014-01-31'}";
            JSONSchemaValidator.validateDocument(doc, schema);
        } catch (JSONValidationException e) {
            fail(e.getMessage());
        }

        doc = "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
              "test:'2014-02-01'}";
        try {
            JSONSchemaValidator.validateDocument(doc, schema);
            fail("expected date out of range");
        } catch (JSONValidationException e) {
            // expected
            assertEquals(
                "date value '2014-02-01' for 'test' is after max date '2014-01-31'",
                e.getMessage()
            );
        }

        doc = "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
              "test:'2013-11-30'}";
        try {
            JSONSchemaValidator.validateDocument(doc, schema);
            fail("expected date out of range");
        } catch (JSONValidationException e) {
            // expected
            assertEquals(
                "date value '2013-11-30' for 'test' is before min date '2013-12-01'",
                e.getMessage()
            );
        }

        try {
            schema = "{document:{type=schema,model=1311.0,namespace=org.hawksoft,id=test,version=1.0}," +
                     "test:\"type=date;format=time;min=0700;max=1230;desc=tbd;example=tbd\"}";
            doc = "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
                  "test:'0745'}";
            JSONSchemaValidator.validateDocument(doc, schema);

            doc = "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
                  "test:'0700'}";
            JSONSchemaValidator.validateDocument(doc, schema);

            doc = "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
                  "test:'1230'}";
            JSONSchemaValidator.validateDocument(doc, schema);
        } catch (JSONValidationException e) {
            fail(e.getMessage());
        }

        doc = "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
              "test:'0659'}";
        try {
            JSONSchemaValidator.validateDocument(doc, schema);
            fail("expected date out of range");
        } catch (JSONValidationException e) {
            // expected
            assertEquals(
                "time value '0659' for 'test' is before min time '0700'",
                e.getMessage()
            );
        }

        doc = "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
              "test:'1231'}";
        try {
            JSONSchemaValidator.validateDocument(doc, schema);
            fail("expected date out of range");
        } catch (JSONValidationException e) {
            // expected
            assertEquals(
                "time value '1231' for 'test' is after max time '1230'",
                e.getMessage()
            );
        }

        try {
            schema = "{document:{type=schema,model=1311.0,namespace=org.hawksoft,id=test,version=1.0}," +
                     "test:\"type=date;format=yyyy;min=1980;desc=tbd;example=tbd\"}";
            doc = "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
                  "test:'1979'}";
            JSONSchemaValidator.validateDocument(doc, schema);
            fail("expected date out of range exception");
        } catch (JSONValidationException e) {
            //expected
            assertEquals(
                "yyyy value '1979' for 'test' is before min yyyy '1980'",
                e.getMessage()
            );
        }

    } // testDateRanges()

    private int _count = 0;

    @Test
    public void testDateThreaded() {
        final int runCount = 500;
        final CyclicBarrier gate = new CyclicBarrier(runCount + 1);

        for (int i = 0; i < runCount; i++) {
            final int iteration = i;
            _count = 0;

            Thread t = new Thread(){
                public void run() {
                    try {
                        gate.await();
                        testDate();
                        testDateFormats();
                        testDateRanges();
                        System.out.println(
                            format("Date Thread %d complete", iteration)
                        );
                        _count++;
                    } catch (Exception e) {
                        fail(e.getMessage());
                    }
                }
            };
            t.start();
        }
        try {
            System.out.println("about to release the threads...");
            gate.await();
            System.out.println("away we go...");
            Thread.sleep(runCount * 15);
            assertEquals(runCount, _count);
        } catch (Exception e) {
            fail(e.getMessage());
        }
    } // testDateThreaded()

    @Test
    public void testDateFormats() {
        String schema =
            "{document:{type=schema,model=1311.0,namespace=org.hawksoft,id=test,version=1.0}," +
            "test:'type=date;format=datetime;desc=tbd;example=tbd'}";
        String doc =
            "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
            "test:'2013-12-31T07:45:23.100 -0600'}";

        try {
            JSONSchemaValidator.validateDocument(doc, schema);
        } catch (JSONValidationException e) {
            fail(e.getMessage());
        }

        try {
            schema = "{document:{type=schema,model=1311.0,namespace=org.hawksoft,id=test,version=1.0}," +
                     "test:'type=date;format=time;desc=tbd;example=tbd'}";
            doc = "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
                  "test:'0745'}";
            JSONSchemaValidator.validateDocument(doc, schema);
        } catch (JSONValidationException e) {
            fail(e.getMessage());
        }
    } // testDateFormats()

    @Test
    public void testEnumSchema() {
        String schema =
            "{document:{type=schema,model=1311.0,namespace=org.hawksoft,id=test,version=1.0}," +
            "test:'type=enum;desc=tbd;example=tbd'}";
        String doc =
            "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
            "test:spades}";

        try {
            JSONSchemaValidator.validateDocument(doc, schema);
            fail("expected exception for missing enum values in schema");
        } catch (JSONValidationException e) {
            // expected
            assertEquals(
                "enum schema for 'test' has no 'values'",
                e.getMessage()
            );
        }
    } // testEnumSchema()

    @Test
    public void testEnum() {
        String schema =
            "{document:{type=schema,model=1311.0,namespace=org.hawksoft,id=test,version=1.0}," +
            "test:'type=enum;values=hearts,spades,diamonds,clubs;desc=tbd;example=tbd'}";
        String doc =
            "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
            "test:spades}";

        try {
            JSONSchemaValidator.validateDocument(doc, schema);
            doc = "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
                  "test:hearts}";
            JSONSchemaValidator.validateDocument(doc, schema);
            doc = "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
                  "test:diamonds}";
            JSONSchemaValidator.validateDocument(doc, schema);
            doc = "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
                  "test:clubs}";
            JSONSchemaValidator.validateDocument(doc, schema);
        } catch (JSONValidationException e) {
            fail(e.getMessage());
        }

        doc = "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
              "test:bogus}";

        try {
            JSONSchemaValidator.validateDocument(doc, schema);
            fail("exception invalid enum value");
        } catch (JSONValidationException e) {
            // expected
            assertEquals(
                "enum 'test' value 'bogus' is not in enumeration 'hearts,spades,diamonds,clubs'",
                e.getMessage()
            );
        }

        doc = "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
              "test:1}";

        try {
            JSONSchemaValidator.validateDocument(doc, schema);
            fail("expected exception invalid enum value");
        } catch (JSONValidationException e) {
            // expected
            assertEquals(
                "'test' is an enum and must be of type string; found 1",
                e.getMessage()
            );
        }
    } // testEnum()

    @Test
    public void testDouble() {
        String schema =
            "{document:{type=schema,model=1311.0,namespace=org.hawksoft,id=test,version=1.0}," +
            "test:'type=double;desc=tbd;example=tbd'}";
        String doc =
            "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
            "test:98.6}";

        try {
            JSONSchemaValidator.validateDocument(doc, schema);
        } catch (JSONValidationException e) {
            fail(e.getMessage());
        }

        doc = "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
              "test:a}";

        try {
            JSONSchemaValidator.validateDocument(doc, schema);
            fail("exception invalid double");
        } catch (JSONValidationException e) {
            // expected
            assertEquals(
                "'test' with value 'a' is not a valid double",
                e.getMessage()
            );
        }
    } // testDouble()

    @Test
    public void testDoubleRanges() {
        String schema =
            "{document:{type=schema,model=1311.0,namespace=org.hawksoft,id=test,version=1.0}," +
            "test:'type=double;min=-101.3;max=20.7;desc=tbd;example=tbd'}";
        String doc =
            "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
            "test:-100.3}";

        Map<Double, Boolean> valueResultMap = new HashMap<>();
        valueResultMap.put(-200.0, false);
        valueResultMap.put(-101.31, false);
        valueResultMap.put(-101.3, true);
        valueResultMap.put(-101.29, true);
        valueResultMap.put(20.65, true);
        valueResultMap.put(20.7, true);
        valueResultMap.put(20.8, false);

        JSONObject oDoc = null;
        JSONObject oSchema = null;
        try {
            oDoc = new JSONObject(doc);
            oSchema = new JSONObject(schema);
        } catch (JSONException e) {
            fail(e.getMessage());
        }

        for (Map.Entry<Double, Boolean> entry : valueResultMap.entrySet()) {
            try {
                oDoc.put("test", entry.getKey());
                try {
                    JSONSchemaValidator.validateDocument(oDoc, oSchema);
                    if (!entry.getValue()) {
                        fail("expected double out of range exception");
                    }
                } catch (JSONValidationException e) {
                    if (entry.getValue()) {
                        // unexpected
                        fail(e.getMessage());
                    } else {
                        // expected
                        assertTrue(e.getMessage().contains("out of range"));
                    }
                }
            } catch (JSONException e) {
                fail(e.getMessage());
            }
        }
    } // testDoubleRanges()

    @Test
    public void testUuid() {
        String schema =
            "{document:{type=schema,model=1311.0,namespace=org.hawksoft,id=test,version=1.0}," +
            "test:'type=uuid;desc=tbd;example=tbd'}";
        String doc =
            "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
            "test:3a20a4d8-98aa-441f-9724-9969575187b2}";

        try {
            JSONSchemaValidator.validateDocument(doc, schema);
        } catch (JSONValidationException e) {
            fail(e.getMessage());
        }

        doc = "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
              "test:a}";

        try {
            JSONSchemaValidator.validateDocument(doc, schema);
            fail("exception invalid double");
        } catch (JSONValidationException e) {
            // expected
            assertEquals(
                "'test' with value 'a' is not a valid uuid",
                e.getMessage()
            );
        }

        doc = "{document:{type=instance,namespace=org.hawksoft,id=test,version=1.0}," +
              "test:3a20a4d8-98aa-441g-9724-9969575187b2}";

        try {
            JSONSchemaValidator.validateDocument(doc, schema);
            fail("exception invalid double");
        } catch (JSONValidationException e) {
            // expected
            assertTrue(e.getMessage().contains("is not a valid uuid"));
        }

    } // testUuid()

    @Test
    public void testUserDefinedTypes() {
        try {
            String userSchema = loadDocument("user-defined/user-defined.jsd");
            String userDoc = loadDocument("user-defined/user-defined-good.json");
            try {
                JSONSchemaValidator.validateDocument(userDoc, userSchema);
            } catch(JSONValidationException e) {
                fail(e.getMessage());
            }

            userSchema = loadDocument("user-defined/missing-attribute.jsd");
            try {
                JSONSchemaValidator.validateDocument(userDoc, userSchema);
                fail("JSONValidationException expected");
            } catch (JSONValidationException e) {
                assertEquals(
                    "document.types entries must contain a 'type' attribute",
                    e.getMessage()
                );
            }

            userSchema = loadDocument("user-defined/missing-ref.jsd");
            try {
                JSONSchemaValidator.validateDocument(userDoc, userSchema);
                fail("JSONValidationException expected");
            } catch (JSONValidationException e) {
                assertEquals(
                    "user defined type 'missing' referenced by attribute 'birthYear' not found",
                    e.getMessage()
                );
            }

            userSchema = loadDocument("user-defined/missing-ref-2.jsd");
            try {
                JSONSchemaValidator.validateDocument(userDoc, userSchema);
                fail("JSONValidationException expected");
            } catch (JSONValidationException e) {
                assertEquals(
                    "user defined type 'year' referenced by attribute 'gradYear' not found",
                    e.getMessage()
                );
            }

        } catch (Exception e) {
            fail(e.getMessage());
        }
    } // testUserDefinedTypes()

    @Test
    public void testImports() {
        try {
            JSONSchemaRepository repo = new JSONSchemaRepository(
                "/org/hawksoft/json/imports/repo"
            );
            assertTrue(repo.getAllSchemas().size() > 0);

            String doc = loadDocument("imports/imports.json");

            JSONSchemaValidator.validateDocument(doc, repo);
        } catch (JSONValidationException | JSONException | IOException e) {
            fail(e.getMessage());
        }
    } // testImports()

    @Test
    public void testStrict() {
        JSONSchemaRepository repo = null;
        try {
            repo = new JSONSchemaRepository(
                "/org/hawksoft/json/strict/repo"
            );
            assertTrue(repo.getAllSchemas().size() > 0);

        } catch (JSONValidationException e) {
            fail(e.getMessage());
        }

        // data run against strict schema should fail
        try {
            String doc = loadDocument("strict/strict-data.json");
            try {
                JSONSchemaValidator.validateDocument(doc, repo);
                fail("exception expected for validation error");
            } catch (JSONValidationException e) {
                //expected
            }
        } catch (JSONException | IOException e) {
            fail(e.getMessage());
        }

        // data run against loose schema should pass
        try {
            String doc = loadDocument("strict/loose-data.json");
            try {
                JSONSchemaValidator.validateDocument(doc, repo);
            } catch (JSONValidationException e) {
                fail(e.getMessage());
            }
        } catch (JSONException | IOException e) {
            fail(e.getMessage());
        }

    } // testStrict()


} // class JSONSchemaValidatorTest
